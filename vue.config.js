module.exports = {
  publicPath: './',
  lintOnSave: false,
  configureWebpack: {
    resolve: {
      extensions: ['.js', '.json', '.vue'],
    },
    devServer: {
      proxy: {
        '/console': {
          target: 'http://localhost:8083/hippo/v1/console',
          changeOrigin: true,
          enable: true,
          pathRewrite: {
            '^/console': ''
          }
        },
        '/passport': {
          target: 'http://localhost:8084/passport/v1/',
          changeOrigin: true,
          enable: true,
          pathRewrite: {
            '^/passport': ''
          }
        }
      }
    }
  }
};
