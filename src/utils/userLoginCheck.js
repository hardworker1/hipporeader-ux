import Axios from 'axios';
import UUID from 'uuid/v4';
import Config from './config'
import {Message} from 'element-ui';
import JSEncrypt from 'jsencrypt';
import globalVal from '../utils/globalVal';

Axios.interceptors.request.use((config)=>{
    const uuid = UUID();
    config.headers['X-request-Id'] = uuid;
    config.headers["Authorization"] = Config.passportToken;
    config.headers["Content-Type"] = "application/json";
    return config;
});

async function userLoginCheck(account, psdCode, loginType){
    var path = `/passport/login_check`;
    var pubKey = globalVal.pubKey;
    let jse = new JSEncrypt();
    jse.setPublicKey(pubKey);
    return Axios.post(path, {
        "account": account,
        "psdCode": jse.encrypt(psdCode),
        "loginType": loginType,
    }).then((response) => {
        if (response.data.data){
            return Promise.resolve(response)
        } else{
            Message({
                type: 'error',
                message: response.data.error
            })
            return Promise.reject(response)
        }
    }).catch((error) => {
        const response = error.response;
        if (response){
            Message({
                type: 'error',
                message: '登入失败，请检查账号是否正确'
            })
        }
    }).finally(() => {
    })
}

export default userLoginCheck;