import http from './http.js';
import Config from './config'
import {Loading, Message} from 'element-ui';
import globalVal from './globalVal';

let loading = true;

async function callCommand(command){
    loading = Loading.service({fullscreen: true, text: '加载中...', background: 'rgba(0,0,0,0.1)'});
    globalVal.loadCount++

    return http.post('/console', {
        apiVersion: Config.consoleVersion,
        data: {
            'command': command
        }
    }).then((response) => {
        if (response.data.data){
            return Promise.resolve(response)
        } else{
            Message({
                type: 'error',
                message: response.data.error
            })
            return Promise.reject(response)
        }
    }).catch((error) => {
        const response = error.response;
        if (response){
            Message({
                type: 'error',
                message: 'Request data error'
            })
        }
    }).finally(() => {
        if (loading && --globalVal.loadCount === 0) {
            loading.close()
        }
    })
}

export default callCommand;