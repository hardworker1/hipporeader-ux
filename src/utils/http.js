import Axios from 'axios';
import UUID from 'uuid/v4';

const http =Axios.create({});

http.interceptors.request.use((config)=>{
    const uuid = UUID();
    config.headers['X-request-Id'] = uuid;
    return config;
});

export default http;