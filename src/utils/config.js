module.exports = {
    appName: 'hippo-reader',
    consoleToken: 'Bearer hippo-console',
    consoleVersion: '1.0',
    consoleUrl: "http://localhost:8083/hippo/v1/console",
    passportToken: "Bearer passport-console",
};