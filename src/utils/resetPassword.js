import Axios from 'axios';
import UUID from 'uuid/v4';
import Config from './config'
import {Message} from 'element-ui';
import globalVal from '../utils/globalVal';
import JSEncrypt from 'jsencrypt'


Axios.interceptors.request.use((config)=>{
    const uuid = UUID()
    config.headers['X-request-Id'] = uuid;
    config.headers["Authorization"] = Config.passportToken;
    config.headers["Content-Type"] = "application/json";
    return config;
});

async function resetPassword(phone, password){
    var path = `/passport/reset`;
    var pubKey = globalVal.pubKey;
    let jse = new JSEncrypt();
    jse.setPublicKey(pubKey);
    return Axios.post(path, {
            "newpassword": jse.encrypt(password),
            "phone": phone
    }).then((response) => {
        if (response.data.data){
            return Promise.resolve(response)
        } else{
            Message({
                type: 'error',
                message: response.data.error
            })
            return Promise.reject(response)
        }
    }).catch((error) => {
        const response = error.response;
        if (response){
            Message({
                type: 'error',
                message: '发送验证码失败'
            })
        }
    }).finally(() => {
    })
}

export default resetPassword;