// 菜单配置
// headerMenuConfig：头部导航配置
// asideMenuConfig：侧边导航配置

const headerMenuConfig = [];

const asideMenuConfig = [
  {
    path: '/bookcase',
    name: '书架',
    icon: 'el-icon-collection'
  },
  {
    path: '/bookstore',
    name: '书城',
    icon: 'el-icon-reading'
  },
  {
    path: '/sort',
    name: '分类',
    icon: 'el-icon-edit-outline',
    children: [
      {
        path: '/1',
        name: '玄幻',
      },
      {
        path: '/2',
        name: '都市',
      },
      {
        path: '/3',
        name: '重生',
      },
      {
        path: '/4',
        name: '穿越',
      },
      {
        path: '/5',
        name: '系统',
      },
      {
        path: '/6',
        name: '言情',
      },
    ]
  },
  {
    path: '/my',
    name: '用户中心',
    icon: 'el-icon-s-custom',
    children: [
      {
        path: '/profile',
        name: '个人信息',
      },
      {
        path: '/history',
        name: '浏览历史',
      },
      {
        path: '/subscription',
        name: '我的关注',
      },
      {
        path: '/production',
        name: '作品管理',
      },
    ]
  }
];

export { headerMenuConfig, asideMenuConfig };
