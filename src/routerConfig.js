// 以下文件格式为描述路由的协议格式
// 你可以调整 routerConfig 里的内容
// 变量名 routerConfig 为 iceworks 检测关键字，请不要修改名称

import HeaderAsideLayout from './layouts/HeaderAsideLayout';
import Bookcase from './pages/Bookcase';
import NotFound from './pages/NotFound';
import BookInfo from './pages/BookInfo';
import BookContent from './pages/BookContent';
import Register from './pages/Register';
import ResetPassword from './pages/ResetPassword';
import userInfo from './pages/userInfo';
import BookStore from './pages/BookStore';
import AuthorInfo from './pages/AuthorInfo';
import ViewHistory from './pages/ViewHistory';
import MySubscription from './pages/MySubscription';
import MyProduction from './pages/MyProduction';
import MyCreating from './pages/MyCreating';
import ProductionInfo from './pages/ProductionInfo';
import CharpterManage from './pages/ChapterManage';
import CreateChapter from './pages/CreateChapter';
import ModifyProduction from './pages/ModifyProduction';
import EditChapter from './pages/EditChapter';
import BookSort from './pages/BookSort';
import Search from './pages/Search';
const routerConfig = [
  {
    path: '/search',
    layout: HeaderAsideLayout,
    component: Search
  },
  {
    path: '/sort/1',
    layout: HeaderAsideLayout,
    component: BookSort,
  },
  {
    path: '/sort/2',
    layout: HeaderAsideLayout,
    component: BookSort,
  },
  {
    path: '/sort/3',
    layout: HeaderAsideLayout,
    component: BookSort,
  },
  {
    path: '/sort/4',
    layout: HeaderAsideLayout,
    component: BookSort,
  },
  {
    path: '/sort/5',
    layout: HeaderAsideLayout,
    component: BookSort,
  },
  {
    path: '/sort/6',
    layout: HeaderAsideLayout,
    component: BookSort,
  },
  {
    path: '/my/profile',
    layout: HeaderAsideLayout,
    component: userInfo,
  },
  {
    path: '/my/subscription',
    layout: HeaderAsideLayout,
    component: MySubscription
  },
  {
    path: '/my/history',
    layout: HeaderAsideLayout,
    component: ViewHistory,
  },
  {
    path: '/my/production',
    layout: HeaderAsideLayout,
    component: MyProduction,
  },
  {
    path: '/edit/chapter',
    layout:EditChapter,
    component:EditChapter,
  },
  {
    path: '/chapter/manage',
    layout: HeaderAsideLayout,
    component: CharpterManage,
  },
  {
    path: '/production/detail',
    layout: HeaderAsideLayout,
    component: ProductionInfo,
  },
  {
    path: '/modify/production',
    layout: HeaderAsideLayout,
    component: ModifyProduction,
  },
  {
    path: '/create/chapter',
    layout: CreateChapter,
    component: CreateChapter,
  },
  {
    path: '/my/creating',
    layout: HeaderAsideLayout,
    component: MyCreating,
  },
  {
    path: '/author',
    layout: HeaderAsideLayout,
    component: AuthorInfo,
  },
  {
    path: '/',
    layout: HeaderAsideLayout,
    component: BookStore,
  },
  {
    path: '/book',
    layout: HeaderAsideLayout,
    component: BookInfo
  },
  {
    path: '/content',
    layout: BookContent,
    component:BookContent
  },
  {
    path: '/register',
    layout: Register,
    component: Register
  },
  {
    path: '/reset',
    layout: ResetPassword,
    component: ResetPassword,
  },
  {
    path: '/bookcase',
    layout: HeaderAsideLayout,
    component: Bookcase,
  },
  {
    path: '/bookstore',
    layout: HeaderAsideLayout,
    component: BookStore,
  },
  {
    path: '/charts',
    layout: HeaderAsideLayout,
    component: NotFound,
    children: [
      {
        path: '/charts/line',
        layout: HeaderAsideLayout,
        component: NotFound,
      },
      {
        path: '/charts/histogram',
        layout: HeaderAsideLayout,
        component: NotFound,
      },
      {
        path: '/charts/bar',
        layout: HeaderAsideLayout,
        component: NotFound,
      },
    ],
  },
  {
    path: '*',
    layout: HeaderAsideLayout,
    component: NotFound,
  },
];

export default routerConfig;
